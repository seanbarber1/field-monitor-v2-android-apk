App Summary

Stage 1;
	1. Flight path is generated based off a rectangle input by the user
		a. During this step, overlap, sidelap, altitude, speed and plan name are designated
	2. Plan is saved once "Save Plan" is selected in the upper left hand corner
	3. Possibility to open plans and clear the map can be selected in the upper left hand corner
	4. Once "Go Fly" is selected, the user is taken to a screen displaying the map with the drone position and flight path
	5. The user must be connected to the drone's wifi and then press the connect button
	6. Once the user is connected, they can start the video stream
	7. Press the arm button once the drone is in a good position to takeoff
	8. Once the arm button is pressed, the takeoff button appears.
	9. Press the takeoff button
	10. Drone does the flight with taking photos causing the video stream to go blank
		a. AT ANY POINT DURING THE FLIGHT, IF THE DRONE MALFUNCTIONS AND YOU WANT TO TAKE MANUAL CONTROL, PRESS DISCONNECT
	11. Drone will go up 5-10 meters before coming home and landing

Stage 2: Implementation of analysis
	TODO!

-Sean Barber 7/17/2018
